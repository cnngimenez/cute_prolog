%% -*- major-mode: prolog-mode; -*-

%% repl.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% Febrero 2024

:- module(repl, [
              repl/0,
              read_line/1
          ]).

/** <module> repl: Read-Eval-Print Loop for cute-Prolog

This modules implement the mechanics of the REPL.

# Personalisations

Prompt, and true and false answers can be changed through defining one of the
multifile predicate: user:user_prompt/0, use:true_answer/1, and user:false_answer/1.

@author Christian Gimenez
@license GPLv3
*/

:- use_module(library(random)).
:- use_module(history).
:- use_module(myprolog).

%! user:user_prompt is det.
%
% The user can define a prefered prompt.
:- multifile user:user_prompt/0.

%! user:true_answer(?Answer: atom) is det
%
% The user preferred answer when the consult returns yes.
:- multifile user:true_answer/1.

%! user:false_answer(?Answer: atom) is det
%
% The user preferred answer when the consult returns no.
:- multifile user:false_answer/1.

%! curline(LastLineRed: codes).
%
% A dynamic predicate that store the characters while reading the input line.
:- dynamic(curline/1).

%! store_char(+Char: number, +Char2: number) is det
%
% Store the char.
%
% There are several exception which char to store and which not.
% For instance, the backspace character should not be stored, instead
% it should erase the last entered character.
%
% Remember that curline/1 store the last character as the first code.
store_char(27, [91, 65]) :- !,
    %% Up
    history_next(Input),
    reverse(Input, Input2),
    
    retractall(curline(_)),
    asserta(curline(Input2)).
store_char(27, [91, 66]) :- !,
    %% Down
    history_previous(Input),
    reverse(Input, Input2),
    
    retractall(curline(_)),    
    asserta(curline(Input2)).
store_char(127, _) :- !,
    %% Backspace.
    curline([_|Line]),
    retractall(curline(_)),
    asserta(curline(Line)).
store_char(C, _) :-
    curline(Line),
    retractall(curline(_)),
    asserta(curline([C|Line])).

echo_char(27, [91, 65]) :- !,
    %% Up
    curline(Input), reverse(Input, Input2),
    write('\e[1K\e[1A'), %% delete line
    prompt,
    format('~s', [Input2]), !.
echo_char(27, [91, 66]) :- !,
    %% Down
    curline(Input), reverse(Input, Input2),
    write('\e[1K\e[1A'), %% delete line
    prompt,
    format('~s', [Input2]), !.
echo_char(127, _) :-
    %% Backspace.
    %% For some reason, the \b character does not work.
    write('\e[1D \e[1D'), !.
echo_char(C, _) :-
    format('~c', [C]), !.

%! read_line(-Line: codes)
%
% Read a line from the input terminal.
read_line(Line) :-
    retractall(curline(_)),
    asserta(curline([])),
    
    read_line_loop,
    history_reset_index,
    
    curline([_|Line1]),
    reverse(Line1, Line).

%! get_next_char(-C: number, -CharList: list) is det.
%
% Get the next char, but considering the escape characters.
%
% If an escape character is received, then the next characters are red too.
% For instance, the up key is represented as the codes [27, 91, 65]. This
% predicate read the escape character (27) in C, and CharList is [91, 65].
get_next_char(C, []) :-
    get_single_char(C),
    C \= 27, !. %% If C is 27, C is 27 and will try the next one.
get_next_char(27, [C]) :-
    get_single_char(C),
    C \= 91, !.
get_next_char(27, [91, C]) :-
    get_single_char(C).

read_line_loop :-
    repeat,
    
    get_next_char(C, Chars),
    store_char(C, Chars),
    echo_char(C, Chars),
    
    C = 13, !.

default_prompt :-
    nl, write('🐈?- ').

%! prompt is det
%
% Show the prompt. It can be the user:user_prompt/0 if defined,
% if not the default_prompt/0.
prompt :-
    user:user_prompt, !.
prompt :-
    default_prompt.

default_true_answers([
                               miau,
                               prrRrRr,
                               miauuuu
                           ]).
default_false_answers( [
                             'GHAA GHAAA',
                             'miau miau miau miau miau',
                             'MIAAUUUUUUUU'
                         ]).

%! print_answer(+Tipo: bool) is det
%
% Print a message declaring the answer of the predicate.
%
% Instead of yes/no we allow the user to use their own message.
% If user:true_answer/1 or user:false_answer/1 are not defined use their respective
% default message.
%
% Deafult is a random message from default_true_answers/1 and default_false_answers/1.
print_answer(true) :-
    user:true_answer(Caption), !,
    nl, write(Caption), nl.
print_answer(true) :- !,
    default_true_answers(Captions),
    random_member(Caption, Captions),
    nl, write('😸 '), write(Caption), nl.
print_answer(false) :-
    user:false_answer(Caption), !,
    nl, write(Caption), nl.
print_answer(false) :- !,
    default_false_answers(Captions),
    random_member(Caption, Captions),
    nl, write('😾 '), write(Caption), nl.

print_error(Error) :-
    nl, write('🙀 '), write(Error), nl,
    fail.

eval_line(Line) :-
    catch(myconsult(Line), Error, print_error(Error)), !,
    %% Answer "yes"
    print_answer(true).
eval_line(_) :-
    %% Answer "no"
    print_answer(false).

repl_loop :-
    repeat,

    prompt,
    read_line(Line), nl,
    
    history_add(Line),
    
    eval_line(Line),

    fail.

repl :-
    history_init,
    repl_loop.
