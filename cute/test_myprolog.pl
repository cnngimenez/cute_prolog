%% -*- major-mode: prolog-mode; -*-

%% test_myprolog.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% Febrero 2024

:- module(test_myprolog, []).

/** <module> test_myprolog: Tests for myprolog module.

@author Christian Gimenez
@license GPLv3
*/


:- begin_tests(myprolog).
:- use_module('myprolog').

test(parameter_with_quotes) :-
    phrase(myprolog:parameter(Param), `'hello world'`, R),
    R = [],
    Param = `hello world`.

test(parameter_list_empty) :-
    phrase(myprolog:parameter_list(Parameters), `     `, R),
    R = [],
    Parameters = [].

test(parameter_list_with_parameters) :-
    phrase(myprolog:parameter_list(Parameters), `  param1,    param2   `, R),
    R = [],
    Parameters = [`param1`, `param2`].

test(parameter_list_with_quotes) :-
    phrase(myprolog:parameter_list(Parameters), `  'param1 param1b', 'param2 param2b'  `, R),
    format('\n~s\n~w\n', [R, Parameters]),
    R = [],
    Parameters = [`param1 param1b`, `param2 param2b`].

test('mypredicate without parameters') :-
    phrase(myprolog:mypredicate(PredName, Params), `test\nrest`, R),
    PredName = `test`,
    Params = [],
    R = `rest`.

test('mypredicate with parameters') :-
    phrase(myprolog:mypredicate(PredName, Params), `test param1, param2\nrest`, R),
    PredName = `test`,
    Params = `param1, param2`,
    R = `rest`.

test(to_predicate_test) :-
    to_predicate(`test param1, param2`, PredName, Params),
    PredName = test,
    Params = [param1, param2].
    
:- end_tests(myprolog).
