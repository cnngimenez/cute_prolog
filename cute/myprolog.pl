%% -*- major-mode: prolog-mode; -*-

%% myprolog.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% Febrero 2024

:- module(myprolog, [
              myconsult/1,
              to_predicate/3,

              cat/1
          ]).

/** <module> myprolog: Syntax parser and evaluation.

@author Christian Gimenez
@license GPLv3
*/

:- use_module(library(dcg/basics)).

predicate_name(PredName) --> string_without("\n ", PredName).

%! parameter(-Parameter: codes)//
%
% @todo Accept \' on quoted parameter.
parameter(Parameter) -->
    %% quoted parameter
    "'", !, string_without("'", Parameter), "'".
parameter(Parameter) --> string_without(", ", Parameter).

parameter_list([]) --> whites, eol, !.
parameter_list([Parameter]) --> whites, parameter(Parameter), whites, eol, !.
parameter_list([Parameter| Rest]) --> whites, parameter(Parameter), whites, ",",
                                    parameter_list(Rest).

mypredicate(PredName, []) --> whites, predicate_name(PredName), whites, eol, !.
mypredicate(PredName, Parameters) --> whites, predicate_name(PredName), whites, string(Parameters), whites, eol, !.

%! to_predicate(+Codes: codes, -PredName: atom, -ParameterList: list)
%
% Convert a code string into the predicate name and a list of parameters.
to_predicate(String, PredName, ParameterList) :-
    phrase(mypredicate(PredNameCodes, ParametersCodes), String, _Rest),
    phrase(parameter_list(ParameterListCodes), ParametersCodes, []),
    maplist(atom_codes, ParameterList, ParameterListCodes),
    atom_codes(PredName, PredNameCodes).

myconsult(String) :-
    to_predicate(String, PredName, Parameters),
    apply(PredName, Parameters).

:- multifile prolog:message//1.

prolog:message(catprint(String)) --> [ '~s'-[String] ].

%! cat(+Filepath: term) is det
%! cat(+LstPaths: list) is det
%
% Show the file contents to the terminal.
cat([FilePaths]) :-
    maplist(cat, FilePaths), !.
cat(FilePath) :-
    read_file_to_string(FilePath, S, []),
    print_message(information, catprint(S)).

