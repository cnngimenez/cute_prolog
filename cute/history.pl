%% -*- major-mode: prolog-mode; -*-

%% history.pl
%% Author: Gimenez, Christian.
%%
%% Copyright (C) 2024 Gimenez, Christian
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%
%% feb 2024

:- module(history, [
              history_init/0,
              history_current_input/1,
              history_add/1,
              history_reset_index/0, history_next/1, history_previous/1,
              history_inputs/1,
              history_suggest/2, history_suggest/3
          ]).

/** <module> history: History for REPL.

Store user inputs to use it again in the REPL.

This library requires to call history_init/0 first. Input can be added with history_add/1,
and retrieved with history_current_input/1.

@author Christian Gimenez
@license GPLv3
*/

%% TODO: use this library:
%% :- use_module(library(persistency)).

%% :- persistent history_inputs(inputs: list).

%! history_inputs(?InputList: list) is det
%
% A list of user inputs.
:- dynamic(history_inputs/1).

%! current_index(?Index: number) is det
%
% The index of the current input.
:- dynamic(current_index/1).

%! history_init is det.
%
% Restart or initialise the history data and index.
history_init :-
    %% db_attach('~/.config/cuteprolog/history.db', []).
    retractall(history_inputs(_)),
    asserta(history_inputs([])),
    history_reset_index.

%! history_add(+Input: term) is det.
history_add(Input) :-
    history_inputs(Previous),
    retractall(history_inputs(_)),
    asserta(history_inputs([Input|Previous])).

%! get_current_input(-CurrentInput: codes).
%
% Return the current history input.
%
% Return false if there is no history.
history_current_input(CurrentInput) :-
    current_index(I),
    history_inputs(Inputs),
    nth1(I, Inputs, CurrentInput).

%! history_reset_index is det
%
% Reset the index of the selected history input.
history_reset_index :-
    retractall(current_index(_)),
    asserta(current_index(1)).

%! add_rotate(+I: number, -I2: number, +Max: number) is det.
%
% I2 is one plus I. If I2 is higher that Max, rotate it to 1.
add_rotate(I, I2, Max) :-
    I2 is I + 1,
    I2 =< Max, !.
add_rotate(_I, 1, _Max).

%! minus_rotate(+I: number, -I2: number, +Max: number) is det.
%
% I2 is one minus I. Rotate to Max if I2 results in zero.
minus_rotate(I, I2, _Max) :-
    I2 is I - 1,
    I2 > 0, !.
minus_rotate(_I, Max, Max).

%! next_index is det
%
% Update the current index to the next one.
next_index :-
    current_index(I),
    history_inputs(Inputs),
    length(Inputs, Max),

    add_rotate(I, I2, Max), 

    retractall(current_index(_)),
    asserta(current_index(I2)).

%! previous_index is det
%
% Update the current index to the previous one.
previous_index :-
    current_index(I),
    history_inputs(Inputs),
    length(Inputs, Max),

    minus_rotate(I, I2, Max), 

    retractall(current_index(_)),
    asserta(current_index(I2)).    

%! history_next(-Input: codes) is det
%
% Return the next element in the history.
history_next(Input) :-
    history_current_input(Input),
    next_index.

%! history_previous(-Input: codes) is det
%
% Return the previous element in the history.
history_previous(Input) :-
    history_current_input(Input),
    previous_index.

%! history_suggest(+Starting: codes, ?Suggestion: codes) is det.
%
% Return the first suggestion from the history list (history_inputs/1).
%
% @see history_suggest/3
history_suggest(Starting, Suggestion) :-
    history_inputs(Inputs),
    history_suggest(Starting, Inputs, Suggestion).

%! history_suggest(+Starting: codes, +List: list, Suggestion) is det.
%
% Return the fist suggestion form List. Starting and List are codes and a list of codes respectively.
%
% @see history_suggest/2
history_suggest(Starting, List, Suggestion) :-
    member(Suggestion, List),
    prefix(Starting, Suggestion).

%% history_suggest(Starting, [Item|List], Item) :-
%%     prefix(Starting, Item), !.
%% history_suggest(Starting, [item|List], Suggestion) :-
%%     history_suggest(Starting, List, Suggestion).
    
